import io
import os

from PIL import Image, ImageFont, ImageDraw, ImageEnhance

# Imports the Google Cloud client library
from google.cloud import vision, language_v1
from google.cloud.vision import types
from google.cloud.language_v1 import enums


def instantiate_vision_client():
    return vision.ImageAnnotatorClient()


def instantiate_language_client():
    return language_v1.LanguageServiceClient()


def read_image_file(filename):
    path = os.path.join(
        os.path.dirname(__file__),
        "resources/{}".format(filename))
    with io.open(path, 'rb') as image_file:
        content = image_file.read()
    return types.Image(content=content)


def read_image_uri(uri):
    image = types.Image()
    image.source.image_uri = uri
    return image


def draw_image(filename, bounds, text=None):
    path = os.path.join(
        os.path.dirname(__file__),
        "resources/{}".format(filename))
    source_img = Image.open(path).convert("RGBA")
    draw = ImageDraw.Draw(source_img)
    for item in bounds:
        draw.rectangle(item, outline="red", width=5)
    source_img.show()


def draw_image_with_text(filename, bounds):
    path = os.path.join(
        os.path.dirname(__file__),
        "resources/{}".format(filename))
    source_img = Image.open(path).convert("RGBA")
    draw = ImageDraw.Draw(source_img)
    x_size, y_size = source_img.size
    for item in bounds:
        pt_1 = (item[0][0][0]*x_size, item[0][0][1]*y_size)
        pt_2 = (item[0][1][0]*x_size, item[0][1][1]*y_size)
        draw.rectangle((pt_1, pt_2), outline="red", width=3)
        draw.text(pt_1,item[1],(255,0,0),)
    source_img.show()


# For Natural Language API
def prepare_document_from_text(text_content):
    type_ = enums.Document.Type.PLAIN_TEXT
    language = "en"
    document = {"content": text_content, "type": type_, "language": language}

    # Available values: NONE, UTF8, UTF16, UTF32
    encoding_type = enums.EncodingType.UTF8

    return document, encoding_type


def prepare_document_from_file(filename):
    path = os.path.join(
        os.path.dirname(__file__),
        "resources/{}".format(filename))
    with open(path, 'r') as file:
        data = file.read().replace('\n', ' ')
    return prepare_document_from_text(data)

# print(open_text_from_file('file.txt'))
