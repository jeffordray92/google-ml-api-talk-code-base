from builtin import *


def detect_text(filename):
    """Detects text in the file."""
    client = instantiate_vision_client()
    image = read_image_file(filename)

    response = client.text_detection(image=image)
    texts = response.text_annotations
    print('Texts:')

    bounds = list()

    for text in texts:
        print('\n"{}"'.format(text.description))

        vertices = (['({},{})'.format(vertex.x, vertex.y)
                    for vertex in text.bounding_poly.vertices])

        bounds.append((eval(vertices[0]), eval(vertices[2])))
        print('bounds: {}'.format(','.join(vertices)))

    draw_image(filename, bounds)


def detect_text_uri(uri):
    """Detects text in the file located in Google Cloud Storage or on the Web.
    """
    client = instantiate_vision_client()
    image = read_image_uri(uri)

    response = client.text_detection(image=image)
    texts = response.text_annotations
    print('Texts:')

    for text in texts:
        print('\n"{}"'.format(text.description))

        vertices = (['({},{})'.format(vertex.x, vertex.y)
                    for vertex in text.bounding_poly.vertices])

        print('bounds: {}'.format(','.join(vertices)))


def detect_text_document(filename):
    client = instantiate_vision_client()
    image = read_image_file(filename)

    response = client.document_text_detection(image=image)
    text = response.text_annotations[0]
    print('Text:')
    print(text.description)
    vertices = ['({},{})'.format(vertex.x, vertex.y)
                for vertex in text.bounding_poly.vertices]
    bounds = [(eval(vertices[0]), eval(vertices[2])), ]
    draw_image(filename, bounds)


def detect_handwriting(path):
    """Detects document features in an image."""
    client = instantiate_vision_client()
    image = read_image_file(filename)

    response = client.document_text_detection(image=image)

    for page in response.full_text_annotation.pages:
        for block in page.blocks:
            print('\nBlock confidence: {}\n'.format(block.confidence))

            for paragraph in block.paragraphs:
                print('Paragraph confidence: {}'.format(
                    paragraph.confidence))

                for word in paragraph.words:
                    word_text = ''.join([
                        symbol.text for symbol in word.symbols
                    ])
                    print('Word text: {} (confidence: {})'.format(
                        word_text, word.confidence))

                    for symbol in word.symbols:
                        print('\tSymbol: {} (confidence: {})'.format(
                            symbol.text, symbol.confidence))
