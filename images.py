from builtin import *


def detect_faces(filename):
    """Detects faces in an image."""
    client = instantiate_vision_client()
    image = read_image_file(filename)

    response = client.face_detection(image=image)
    faces = response.face_annotations

    # Names of likelihood from google.cloud.vision.enums
    likelihood_name = ('UNKNOWN', 'VERY_UNLIKELY', 'UNLIKELY', 'POSSIBLE',
                       'LIKELY', 'VERY_LIKELY')
    print('Faces:')

    bounds = list()
    for face in faces:
        print('anger: {}'.format(likelihood_name[face.anger_likelihood]))
        print('joy: {}'.format(likelihood_name[face.joy_likelihood]))
        print('surprise: {}'.format(likelihood_name[face.surprise_likelihood]))

        vertices = (['({},{})'.format(vertex.x, vertex.y)
                    for vertex in face.bounding_poly.vertices])
        bounds.append((eval(vertices[0]), eval(vertices[2])))

        print('face bounds: {}'.format(','.join(vertices)))
    draw_image(filename, bounds)


def detect_labels(filename):
    """Detects labels in the file."""
    client = instantiate_vision_client()
    image = read_image_file(filename)

    response = client.label_detection(image=image)
    labels = response.label_annotations
    print('Labels:')

    for label in labels:
        print(label.description)
        print(label.score)


def detect_landmarks(filename):
    """Detects landmarks in the file."""
    client = instantiate_vision_client()
    image = read_image_file(filename)

    response = client.landmark_detection(image=image)
    landmarks = response.landmark_annotations
    print('Landmarks:')

    for landmark in landmarks:
        print(landmark.description)
        for location in landmark.locations:
            lat_lng = location.lat_lng
            print('({}, {})'.format(lat_lng.latitude, lat_lng.longitude))


def detect_logos(filename):
    """Detects logos in the file."""
    client = instantiate_vision_client()
    image = read_image_file(filename)

    response = client.logo_detection(image=image)
    logos = response.logo_annotations
    print('Logos:')

    bounds = list()

    for logo in logos:
        print(logo.description)
        vertices = (['({},{})'.format(vertex.x, vertex.y)
                    for vertex in logo.bounding_poly.vertices])
        bounds.append((eval(vertices[0]), eval(vertices[2])))

    draw_image(filename, bounds)


def detect_objects(filename):
    """Localize objects in the local image."""
    client = instantiate_vision_client()
    image = read_image_file(filename)

    objects = client.object_localization(
        image=image).localized_object_annotations

    print('Number of objects found: {}'.format(len(objects)))

    bounds = list()

    for item in objects:
        print('\n{} (confidence: {})'.format(item.name, item.score))

        vertices = (['({},{})'.format(vertex.x, vertex.y)
                    for vertex in item.bounding_poly.normalized_vertices])

        bounds.append(((eval(vertices[0]), eval(vertices[2])), item.name))

    draw_image_with_text(filename, bounds)


def detect_safe_search(uri):
    """Detects unsafe features in the file."""
    client = instantiate_vision_client()
    image = read_image_uri(uri)

    response = client.safe_search_detection(image=image)
    safe = response.safe_search_annotation

    # Names of likelihood from google.cloud.vision.enums
    likelihood_name = ('UNKNOWN', 'VERY_UNLIKELY', 'UNLIKELY', 'POSSIBLE',
                       'LIKELY', 'VERY_LIKELY')
    print('Safe search:')

    print('adult: {}'.format(likelihood_name[safe.adult]))
    print('medical: {}'.format(likelihood_name[safe.medical]))
    print('spoofed: {}'.format(likelihood_name[safe.spoof]))
    print('violence: {}'.format(likelihood_name[safe.violence]))
    print('racy: {}'.format(likelihood_name[safe.racy]))

detect_safe_search("IMG_4398.JPG")
